const T2I_RUNWAY_API_URL_BASE = 'http://127.0.0.1:8003/';


document.addEventListener('DOMContentLoaded', function () {
    setInterval(getResult, 3000);

    this.input_elem = document.getElementById('input');
    this.output_elem = document.getElementById('output');

    this.input_elem.oninput = postQuery;

    this.output_elem.onerror = function () {
        this.style.display = 'none';
    };

    let self = this;

    function getResult() {
        if (self.input_elem.value) {
            fetch(T2I_RUNWAY_API_URL_BASE + 'data')
                .then(response => response.json())
                .then(response_json => {
                    const {result} = response_json;
                    if (result) {
                        self.output_elem.style.display = "block";
                        self.output_elem.src = result
                    } else {
                        self.output_elem.display = 'none';
                    }
                })
        } else {
            self.output_elem.style.display = "none";
        }
    }

    function postQuery() {
        let query = self.input_elem.value;
        if (query) {
            fetch(T2I_RUNWAY_API_URL_BASE + 'query', {
                method: 'POST',
                headers: {
                    Accept: 'application/json',
                    'Content-Type': 'application/json',
                },
                body: JSON.stringify({
                    "caption": query
                })
            })
                .then(response => response.json())
                .then(response_json => {
                    const {result} = response_json;
                    if (result) {
                        self.output_elem.style.display = "block";
                        self.output_elem.src = result
                    } else {
                        self.output_elem.display = 'none';
                    }
                })
        } else {
            self.output_elem.style.display = "none";
        }
    }

});